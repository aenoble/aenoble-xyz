#!/bin/just -f

src_dir  := "src"
out_dir  := "dist"
vale_dir := "vale"
repo_url := "git@codeberg.org:aenoble/aenoble-xyz.git"

build *args:
	yarn
	npx statcodon {{ args }}

bf: (build "-f")

clean:
	rm -rf {{out_dir}}

alias proselint := vale

vale files=src_dir:
	-vale {{files}} --config {{vale_dir}}/vale.ini

prettify files=src_dir:
	npx prettier {{ if files == src_dir {""} else if files == "." {""} else {"--ignore-path=/dev/null"} }} -w {{files}}

prettifyssg: (prettify ".")
	# Alphabetically sort vale dictionary
	@IGN=$(sort -f {{vale_dir}}/styles/Spelling/ignore.txt) && echo "$IGN" >{{vale_dir}}/styles/Spelling/ignore.txt && echo done

publish: clean build
	#!/bin/sh
	cp .domains {{out_dir}}/.domains
	cd {{out_dir}}
	git init -b pages
	git remote add origin {{repo_url}}
	git add .
	git commit -m "Deployment at $(date)"
	git push -f -u origin pages
