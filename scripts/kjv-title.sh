# https://github.com/arleym/kjv-markdown
# Changes the first heading `# Genesis` to a template option
# `title: Genesis`, and removes book titles from chapter titles.

ch() {
  CONTENT=$(cat "$1")
  TITLE=$(echo "$CONTENT" | sed '1s/^..//
                                 q')
  CONTENT=$(echo "$CONTENT" | tail -n +3)

  # Outputs a Markdown link item to stderr
  RF=$(echo "$1" | cut -d/ --fields 1)
  RB=$(basename "$1")
  RB=${RB:0:-3}
  [ "$RF" = "src" ] && echo "$RB. [$TITLE](/${1:4:-3}.html)" >&2

  # Constructs document
  echo ---
  echo title: "$TITLE"
  echo ---
  echo
  echo "$CONTENT" | awk '/^## / { sub("'" $TITLE"'| Psalm| Song of Solomon| Matthew| Mark| Luke| John", "", $0); print }
                        !/^## / { print }'
}

# Constructs index file. Temp file is used to prevent overwriting above.
TMP_INDEX=$(mktemp)
echo --- >> $TMP_INDEX
echo 'title: "<span style=\"font-variant: small-caps;\">The Holy Bible</span>, King James Version"' >> $TMP_INDEX
echo --- >> $TMP_INDEX
echo >> $TMP_INDEX
echo '> <cite>As obtained from [this repository](https://github.com/arleym/kjv-markdown).</cite>' >> $TMP_INDEX
echo >> $TMP_INDEX
echo "Read the word of god on some random guy's website!" >> $TMP_INDEX
echo >> $TMP_INDEX

for f in src/where/kjv/*
do NC=$(ch "$f" 2>> $TMP_INDEX); echo "$NC" > "$f"
done

mv $TMP_INDEX src/where/kjv/index.md
