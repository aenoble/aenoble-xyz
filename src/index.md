---
title: Index page
template: base
---

<style>
html, body {margin: 0 auto}
html, body, main { height: 100% }
</style>

<div style="
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
">
<div class="card">

<div class="card__title" style="font-size: larger">Alex Noble</div>

```pug
- var stallman = '“I use Linux as my operating system,” I state proudly to the unkempt, bearded man. He swivels around in his desk chair with a devilish gleam in his eyes, ready to mansplain with extreme precision. “Actually,” he says with a grin, “Linux is just the kernel. You use GNU+Linux!” I don’t miss a beat and reply with a smirk, “I use Alpine, a distro that doesn’t include the GNU coreutils, or any other GNU code. It’s Linux, but it’s not GNU+Linux.”\n\nThe smile quickly drops from the man’s face. His body begins convulsing and he foams at the mouth and drops to the floor with a sickly thud. As he writhes around he screams “I-IT WAS COMPILED WITH GCC! THAT MEANS IT’S STILL GNU!” Coolly, I reply “If Windows was compiled with gcc, would that make it GNU?” I interrupt his response with “And work is being made on the kernel to make it more compiler-agnostic. Even you were correct, you won’t be for long.”\n\nWith a sickly wheeze, the last of the man’s life is ejected from his body. He lies on the floor, cold and limp. I’ve womansplained him to death.\n\nHeh'

p
  | Socially conscious asshole, annoying&nbsp;
  span(title=stallman) Linux user, Free Software
  | &nbsp;and privacy advocate.
```

Also xkcd reader and very occasional book reader. Extremely occasional.

Pronouns he/she/per/they, I don't particularly care what you call me.

I don't have much on this website.

</div></div>
