---
title: 404
template: base
---

```pug
include _includes/header.pug

big There isn't a page here.#{" "}
  a(href="/") Go home.

```
