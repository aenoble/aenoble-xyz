---
title: Posts
template: default
na: true
---

There's more in an archive somewhere, but I'll get to that later.

<!-- - [the cashless policy](/posts/2022/06/cashless.html) (June 6, 2022) -->

- [tech ed and edtech](/posts/2022/06/teched.html) (June 23, 2022)
