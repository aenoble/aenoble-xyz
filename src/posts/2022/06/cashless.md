---
title: the cashless policy
date: June 6, 2022
---

This post isn't particularly well-structured, but I was annoyed and had
to write something.

I had the unfortunate experience today of being trapped outdoors in a
park for about five hours, where I was prohibited from bringing my own
food or drink inside. So obviously I needed to _buy_ water. But that's
not the issue. Quite simply:

_They don't accept cash._

The night before, I acquired a $50 prepaid card for the _sole purpose_
of purchasing water and food. With that I bought about _one small
plastic water bottle_.

Again, what matters isn't the popcorn policy; it's the cashless policy.
Their stated reason for this cashless policy is this:

> By going cashless, we are able to conduct contact-less transactions,
> and it’s faster, safer and always secure.

<big>I'd just like to interject for a moment. What you're referring to
as---</big>

Heck off.

<strong id="contactless">One.</strong> Why are contactless transactions
so important? The chance of spreading disease through a brief contact
with Treasury-issue fabric paper is very low. Even if it were reasonably
high, are we sure that the card reader is cleaner? Because I can tell
you with a common sense knowledge of germ theory that _it ain't_.

**Two.** I will acknowledge that it's faster to wave my phone at the
card reader, but the extra ten seconds spent counting a few bills and
giving change is not enough reason to force me to bring a card, thanks.

**Three.** [It's not significantly safer](#contactless)

**Four.** Online systems to manage finances are _never_ secure. It's
apparently a shock to hear this for most people! But the moment
something is done over the Internet, it is certifiably fucked. There is
a good reason that elections are not done online.

Anyway.
