---
title: tech ed and edtech
datemr: "2022-06-23T06:37:13-04:00"
license: CC-BY-ND
copyright: 2021–2022
---

Over the past two years I've minorly rallied around using libre software
at school. And I've written long articles about why we need to do that.
I've never published them, unfortunately, and this article is
essentially a culmination of all of my thoughts in them.

Previously I would fault the school district for doing what made sense
with "workplace technologies education." However, I want to convince you
and not shame you, so now I really fault the companies for their
wrongful and deceptive practices.

<!-- </div> -->

## The Problem

At my school, and many organizations across the modern world, technology
companies have free rein over the programs and software that people use.
This rips away the end user's freedoms by usually restricting them to
not reverse engineer, modify, or distribute the program themselves. They
often spy on you, lock you in, and leave you without security updates.
And they can be stuck in the past with a void of modern features.

### Privacy

As you've undoubtedly heard by now, while every application needs some
amount of data to function, today's programs largely collect data for
profit. Large data-brokering companies like Microsoft, Apple, Google,
and Amazon are harvesting as much information as possible about you,
online and in real life. What do they do with this information? Simply
advertise to you.

Many say, "Oh, no, they'll show me an ad or two..." However, perhaps
_most_ people believe the data collected is excessive for that. With
Apple's (not so) recent addition of privacy labels, and Google's more
recent following, many people have seen the true nature of the subject.
Nonetheless allow me quickly explain how modern trackers work.

In the realm of mobile apps, tracking is done through essentially the
same method as below, except with significantly more potential
information to gather.

<figure>

![Data Linked to You: Location, Contacts, Search History, Usage Data, Other Data, Contact Info, User Content, Identifiers, Diagnostics](google-pl.png)

<figcaption>The privacy label for Google Classroom</figcaption>

<figcaption role="note" aria-label="'Identifiers'">“Identifiers” refers to a device fingerprint and your user account</figcaption>

</figure>

This is however mostly out of scope.

Many websites themselves run ads, and many more websites include
analytics code on their page to see aggregate information about their
visitors. Both of these are often outsourced to a company like Google,
Yandex, or Amazon. This code collects information about the specific web
browser (a "fingerprint") that visits the page, and links it with the
page visited. On these companies' servers, they can link multiple web
browsers that have substantially similar fingerprints and compile a list
of pages you've visited. This allows different algorithms to extrapolate
your interests, hobbies, and even struggles. When advertisers set up
campaigns, they select a group which they would like to target, and the
companies' servers will show you ads which best match your profile.

Alternatively, you use Google Chrome or another spyware browser, and
Google assigns a unique ID to you and swiftly sweeps all of your data
away :boat:.

Companies also collect data from other platforms, like social media, or
link it with their own services like Google does. And all of this
tracking includes creepily detailed profiles. Messaging app Signal
[ran][signal] an Instagram ad campaign to try to expose it, which
Facebook hastily banned.

<figure>

<!-- vale off -->

<img alt="Three squares with blue background with the Signal logo in the corner. The first states: “You got this ad because you’re a K-pop–loving chemical engineer. This ad used your location to see you’re in Berlin. And you have a new baby. And just moved. And you’re really feeling those pregnancy exercises lately.”" src="/pix/signal-ig-kpop-teacher-gp.png"></img>

<!-- vale on -->

<figcaption>Some of the examples of Signal’s ads from their blog post</figcaption>

</figure>

Necessary data collection for the basic function of services is bad when
the data is controlled by a third-party completely out of the control of
the school. Companies will tell you they don't abuse or hand out student
data and that they are compliant with a host of regulations. These
claims can't be verified, however, and should not be trusted.

### Lock-in

Switching between multiple applications is a hassle for anyone.
Companies like to make proprietary formats for storing data, even when
similar formats already existed. And in the more modern world of cloud
services, your data is both in a custom format and locked away behind
the glass of your screen (and their servers).

For example, Microsoft Word used its own file format rather than the one
used by WordPerfect at the time. While this did allow them to add more
formatting features to documents, at their core, these programs allowed
you to store formatted text: the breakage of an extension rather than a
whole new format would be heavily limited.

Initially, the earliest word processors were more concerned with getting
the initial checklist down: word processing (duh), typesetting,
printing... Only later is this for the univocal reason of companies'
wanting you to use _their_ app for that one purpose. Microsoft _did_
intentionally used their own closed format once they had dominated the
office software market, effectively preventing interoperability with
competitors. It can be argued that Microsoft has made the Office formats
open, but the [specification for Office OpenXML][ecma376] is in 4 parts,
totaling 6730 pages, and the specification is designed to support every
historical feature and bug of Microsoft Office products dating back to
their initial release. In contrast, the [international standard
OpenDocument format][iso26300] totals 1106 pages.

### Security

Where a program is closed source, you put a lot of trust in that
company's security teams. If a program _can_ reasonably be exploited, it
_will_ be. You are trusting essentially that their security teams are
better than everyone else's, and that there are not any intentional
design flaws, i.e. backdoors.

To fill in the blanks, for example: only Microsoft can review the source
for Microsoft Exchange Server (a common email program), and it has major
vulnerabilities as a result allowing _administrative_ access to the
system.

Security vulnerabilities are inevitable in all software. When these
issues cannot be proactively discovered, however, it leads to actual
consequences. Data breaches are commonplace among service providers of
all types. Companies are often negligent and don't fix security
vulnerabilities in a timely manner. Often, partly because the source
code is closed, these vulnerabilities are discovered far too late.

Each week there are several data breaches, and the number year on year
keeps increasing. Data breaches exposing even seemingly little data can
contribute to identity theft. [School][zdnet] or [edtech][denmark] hacks
are fairly common at this point. And child identity theft is on the
rise. _(Sorry, credit monitoring won't save you now.)_

Also, with the licensing model of a lot of software, companies withhold
important security updates behind a paywall of "buy the latest version."
For some systems, like your phone or Chromebook, there _are_ no security
updates after a set amount of time: 6 years for some Chromebooks, and
usually 1 to 3 for mobile devices.

### Corporate bulls\-\-\-

When you use any given service, the terms of service are a standard form
contract where you have no negotiating power. The contract is thus
heavily swayed towards whatever's good for them.

Generally, a data breach is not the direct result of negligence.
However, when a service is penetrated and user data is leaked, this
causes direct harm to a handful of the people involved. It can cause
annoyance for many people when their phone number is publicly linked
with their identity, and the more vulnerable of the group may fall for
phishing scams.

One of the recurring provisions of these contracts is a class action
waiver/binding arbitration provision. This means that you have no right
to make or join a class action lawsuit, and you must resolve all
disputes through individualized arbitration. Some will say to let people
make their own contracts, but when this is forced upon you, it is simply
unconscionable. A glaring example of that is [MySchoolBucks][msb], a
popular school _payment processor_. I would hope this is somehow illegal
because of financial regulations, but challenging the provision is more
work than most people are up for.

### Centralization

In the same vein as above, features like sharing between users is
heavily centralized with a lot of services like Google Docs. Often
companies refuse to support protocols like WOPI which are tightly
integrated with ONLYOFFICE, Collabora Online and Microsoft's Office
Online Server.

Also, today, when you log into a website or app, you likely use one of
these buttons:

<figure>

![Single sign-on buttons for Google, Clever, Facebook, and Apple](sso-sign-in.png)

</figure>

You'll notice the only options available are fairly well-known, bar
Clever, who provides single sign-on to schools specifically. Generally
the options are Google, Facebook, Twitter, Apple, and/or GitHub.
Noticeably less sites today have username and password
login.[^user-pass] This means for a large number of services,
alternative single sign-on providers are not usable, except in _super_
enterprise systems.

[^user-pass]:
    I _am_ aware that SSO is much more secure than a username/password
    setup for the majority of users. I don't believe this will decrease
    the number of normal users using SSO instead due to its convenience.
    The tradeoff thus only disadvantages users outside of these
    platforms.

### School issues

At the beginning of this article, I noted that I do not want to shame
the school, but there are some legitimate things to call out which make
the problem worse. This is coming from the perspective of a student at
Gateway Regional High School.

First, technical issues.

On school Chromebooks here, the geolocation permission is enabled for
all sites. This is dangerous! While your location within the school is
not exactly classified, it can also show the exact position of students'
homes to a malicious site. I also want to note that our district has
completely blocked installing any extensions like uBlock Origin, which
block ads and trackers, and also include a few other privacy-enhancing
features.

```pug
include _includes/geolocation.pug
```

Second, education issues.

<span id="password__"></span>

Password security at our school is laughable. At face value, they are
eight characters long; in real value, six.[^password] Either way, these
passwords are not the slightest bit hard to crack. I really like this
comic for explaining it:

[^password]:
    The last two digits---year of graduation---can be derived from the
    email address and is generally public information.

<figure>

![](/pix/xkcd-936.png)

<figcaption><cite><a href="https://xkcd.com/936/">xkcd 936, “Password Strength”</a></cite></figcaption>

</figure>

Also, I received a lengthy "Acceptable Use Policy Violation" email when
it was discovered that my password changed. They changed it back. If
they have the power to change the password at a whim, why is it a
problem when I change it myself?

### References

- <cite id="c.1.signal">[Signal messenger: "The Instagram ads you will never see"](https://signal.org/blog/the-instagram-ads-you-will-never-see/)</cite>
- <cite id="c.1.zdnet">[_ZDNet_: "2020 was a 'record-breaking' year in US school hacks, security failures"](https://www.zdnet.com/article/2020-was-a-record-breaking-year-in-us-school-hacks-security-failures/)</cite>
- <cite id="c.1.denmark">[_TV2 News_: "Schools and institutions hit by major data leak - 750,000 children and adults' data exposed" (in Danish)](https://nyheder.tv2.dk/samfund/2022-06-13-skoler-og-institutioner-ramt-af-stort-datalaek-750000-boern-og-voksnes)
  ([translation](https://paste.sethforprivacy.com/?82e0d87c0b9643a4#5kpz5ou7YqZU2F9k4E1zPj2pzFKy4c1tzqxcmS5kAkTV))</cite>
- <cite id="c.1.msb"><a rel="nofollow noreferer noopener" href="https://www.myschoolbucks.com/ver2/etc/getterms">MySchoolBucks
  Terms of Service</a></cite>
- <cite id="c.1.ecma376"><a rel="nofollow noreferer noopener" href="https://www.ecma-international.org/publications-and-standards/standards/ecma-376/">ECMA-376</a>
  & ISO/IEC 29500-{1,2,3,4}:2015, Office Open XML file formats</cite>
- <cite id="c.1.iso26300">ISO/IEC 26300-{1,2,3}:2015 from the
  <a rel="nofollow noreferer noopener" href="https://standards.iso.org/ittf/PubliclyAvailableStandards/index.html">Publicly
  Available Standards</a> list</cite>

[signal]: #c.1.signal
[msb]: #c.1.msb
[ecma376]: #c.1.ecma376
[iso26300]: #c.1.iso26300
[zdnet]: #c.1.zdnet
[denmark]: #c.1.denmark

#### Further reading

<!-- - <cite id="c.1.offenders">[My followup article discussing selected data offenders](teched_offenders.html)</cite> -->

- <cite id="c.1.mkup">["This Private Equity Firm Is Amassing Companies That Collect Data on America's Children"](https://themarkup.org/machine-learning/2022/01/11/this-private-equity-firm-is-amassing-companies-that-collect-data-on-americas-children),
  by Todd Feathers from _The Markup_</cite>
- <cite id="c.1.kumar">["WhatsApp and the Domestication of Users"](https://seirdy.one/posts/2021/01/27/whatsapp-and-the-domestication-of-users/),
  by Roman Kumar</cite>

[mkup]: #c.1.mkup

---

<h3 style="display: none"></h3>

Hopefully by now you are convinced that the applications underlying
"workplace technologies education" are highly problematic. By teaching
exclusively these softwares, students will in the future be biased
towards these softwares. Many schools teach Google products, so students
will entrust their emails with Gmail, their documents with Google Docs,
their personal photos with Google Photo, and so on---and they will be
ignorant, perhaps numb, towards the dangerous nature of these. The few
who start their own businesses will force these apps on their employees.
This perpetuates the privacy-invasive, corporate subservience business
model indefinitely.

So how can we fix it?

## The Solution

### Free/libre software

"Free software" or "libre software" refers to programs where people have
the rights to review, modify, and distribute them. They are much more
flexible, generally more private and secure, and usually don't lock you
in. They also generally come at zero direct cost.

The term _libre_ (the more traditional cliché being "free as in
freedom") is generally preferred in some groups to contrast with
_gratis_ (or "free as in beer").

The Four Freedoms are usually recited as follows:

> 0. The freedom to run the program as you wish, for any purpose.
> 1. The freedom to study how the program works, and change it so it
>    does your computing as you wish. Access to the source code is a
>    precondition for this.
> 2. The freedom to redistribute copies so you can help others.
> 3. The freedom to distribute copies of your modified versions to
>    others. By doing this you can give the whole community a chance to
>    benefit from your changes. Access to the source code is a
>    precondition for this.

---[The GNU Project](https://www.gnu.org/philosophy/free-sw.html)

#### How does libre software solve these issues?

First, free software is generally more private than most corporate
programs. Including spyware would be a major blow to the credibility of
the software because of its inherent transparency. With this
transparency, unlike proprietary programs, there is no need to dig to
find privacy invasions---search the source code for common services or
read through the changelog. If there is a problem, anyone is allowed to
point out the issue, and modify the program and distribute their
changes.

As for lock-in, free programs often use established open formats
instead. Even if their formats are not well-documented, the code is
available to anyone, and the format can be implemented by another
program without complex, potentially illegal reverse-engineering. Free
software doesn't solve the problem of corporate centralization, but
engineering decisions can---[see below.](#decentralization)

Libre software also solves many security issues. Because the source code
is available to all, security experts can study it and identify
potential security holes and report them privately. Waiting for a
company to fix the problem themselves can be removed from the workflow
altogether if security experts fix it themselves.

Finally, the software licenses behind most projects are the _entire_ set
of terms you must obey for the software; additional restrictions added
without relicensing the software are non-binding. The contract is made
at one time, and cannot be changed without overwhelming support from
contributors.

#### Other benefits

Libre software is the backbone of a lot of the world's most important
systems. Most of the Internet runs on Apache or nginx on Linux. The only
"enterprise-scale" applications that don't support Linux and/or BSD
either are Microsoft products or are on life support from their
developers.

In a computer science class, libre software allows students to study how
a well-writen program is structured and functions.

As a secondary but important benefit, most libre software is gratis in
price.

### Decentralization

Implementing decentralization in programs is another important step
towards maintaining user freedoms.

As mentioned above, Google refuses to support WOPI for their
applications. Since ONLYOFFICE and Collabora Online, libre software
projects with far less funding than Google products, could implement
Microsoft's protocol, there is no reason why Google could not do so.
Parties which implement things _properly_ are favored in decentralized
systems.

Instead of a centralized authentication flow inside an app, a flow
generally allows users to authenticate with any given identity
provider[^m-auth]. This is already possible with OpenID and its later
successor OpenID Connect.

[^m-auth]:
    In reality, a protocol like ActivityPub (Mastodon, Pleroma, etc.)
    will have its own user authentication scheme, but this does not stop
    an individual implementation from allowing other systems. As an
    example, the Matrix protocol allows any authentication scheme that a
    given client app supports, and only defines a few _core_ schemes.

<!-- :cricket: not done -->

---

<!-- ...tbf -->

Software ethics only solves half the problem. The other half is policy.
I _will_ write another article about this consiering this article is
already longer than most people's attention spans support.
