#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void FizzBuzz(int i) {
  bool fizz = (i % 3 == 0);
  bool buzz = (i % 5 == 0);

  unsigned short ta = (fizz ? 4 : 0) + (buzz ? 4 : 0);
  if (ta == 0) printf("%i\n", i);
  else {
    char *out = malloc(ta);
    strcpy(out, "");
    if (fizz) strcat(out, "Fizz");
    if (buzz) strcat(out, "Buzz");
    printf("%s\n", out);
    free(out);
  }
}

int main() {
  for (unsigned short i = 1; i <= 100; i++) {
    FizzBuzz(i);
  }

  return 0;
}
