---
title: "<span style=\"font-variant: small-caps;\">The Holy Bible</span>, King James Version"
---

> <cite>As obtained from [this repository](https://github.com/arleym/kjv-markdown).</cite>

Read the word of god on some random guy's website!

01. [Genesis](/where/kjv/01.html)
02. [Exodus](/where/kjv/02.html)
03. [Leviticus](/where/kjv/03.html)
04. [Numbers](/where/kjv/04.html)
05. [Deuteronomy](/where/kjv/05.html)
06. [Joshua](/where/kjv/06.html)
07. [Judges](/where/kjv/07.html)
08. [Ruth](/where/kjv/08.html)
09. [1 Samuel](/where/kjv/09.html)
10. [2 Samuel](/where/kjv/10.html)
11. [1 Kings](/where/kjv/11.html)
12. [2 Kings](/where/kjv/12.html)
13. [1 Chronicles](/where/kjv/13.html)
14. [2 Chronicles](/where/kjv/14.html)
15. [Ezra](/where/kjv/15.html)
16. [Nehemiah](/where/kjv/16.html)
17. [Esther](/where/kjv/17.html)
18. [Job](/where/kjv/18.html)
19. [Psalms](/where/kjv/19.html)
20. [Proverbs](/where/kjv/20.html)
21. [Ecclesiastes](/where/kjv/21.html)
22. [The Song of Solomon](/where/kjv/22.html)
23. [Isaiah](/where/kjv/23.html)
24. [Jeremiah](/where/kjv/24.html)
25. [Lamentations](/where/kjv/25.html)
26. [Ezekiel](/where/kjv/26.html)
27. [Daniel](/where/kjv/27.html)
28. [Hosea](/where/kjv/28.html)
29. [Joel](/where/kjv/29.html)
30. [Amos](/where/kjv/30.html)
31. [Obadiah](/where/kjv/31.html)
32. [Jonah](/where/kjv/32.html)
33. [Micah](/where/kjv/33.html)
34. [Nahum](/where/kjv/34.html)
35. [Habakkuk](/where/kjv/35.html)
36. [Zephaniah](/where/kjv/36.html)
37. [Haggai](/where/kjv/37.html)
38. [Zechariah](/where/kjv/38.html)
39. [Malachi](/where/kjv/39.html)
40. [The Gospel According to Matthew](/where/kjv/40.html)
41. [The Gospel According to Mark](/where/kjv/41.html)
42. [The Gospel According to Luke](/where/kjv/42.html)
43. [The Gospel According to John](/where/kjv/43.html)
44. [Acts](/where/kjv/44.html)
45. [Romans](/where/kjv/45.html)
46. [1 Corinthians](/where/kjv/46.html)
47. [2 Corinthians](/where/kjv/47.html)
48. [Galatians](/where/kjv/48.html)
49. [Ephesians](/where/kjv/49.html)
50. [Philippians](/where/kjv/50.html)
51. [Colossians](/where/kjv/51.html)
52. [1 Thessalonians](/where/kjv/52.html)
53. [2 Thessalonians](/where/kjv/53.html)
54. [1 Timothy](/where/kjv/54.html)
55. [2 Timothy](/where/kjv/55.html)
56. [Titus](/where/kjv/56.html)
57. [Philemon](/where/kjv/57.html)
58. [Hebrews](/where/kjv/58.html)
59. [James](/where/kjv/59.html)
60. [1 Peter](/where/kjv/60.html)
61. [2 Peter](/where/kjv/61.html)
62. [1 John](/where/kjv/62.html)
63. [2 John](/where/kjv/63.html)
64. [3 John](/where/kjv/64.html)
65. [Jude](/where/kjv/65.html)
66. [Revelation](/where/kjv/66.html)
