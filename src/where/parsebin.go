package main

import (
	"os"
	"strconv"
	"unicode/utf8"
)

func main() {
	if len(os.Args) < 2 {
		return
	}

	s := os.Args[1]
	var chunks = []string{}
	for len(s) > 8 {
		var i = 8
		for i >= 8-utf8.UTFMax && !utf8.RuneStart(s[i]) {
			i--
		}
		chunks = append(chunks, s[:i])
		s = s[i:]
	}
	if len(s) > 0 {
		chunks = append(chunks, s)
	}

	bytes := []byte{}
	for _, t := range chunks {
		v, _ := strconv.ParseInt(t, 2, 8)
		bytes = append(bytes, byte(v))
	}

	os.Stdout.Write(bytes)
}
