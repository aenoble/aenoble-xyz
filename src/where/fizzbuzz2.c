#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void FizzBuzz(int i) {
  bool fizz = (i % 3 == 0);
  bool buzz = (i % 5 == 0);

  if (fizz + buzz == 0) printf("%i\n", i);
  else {
    char *out = malloc(8);
    strcpy(out, "");
    if (fizz) strcat(out, "Fizz");
    if (buzz) strcat(out, "Buzz");
    printf("%s\n", out);
    free(out);
  }
}

int main() {
  for (unsigned short i = 1; i <= 100; i++) {
    FizzBuzz(i);
  }

  return 0;
}
