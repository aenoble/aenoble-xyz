*Originally committed August 18, 2021.*

This is a little something I use on my assignments. Ha ha.

---

I have sent you keysmash.

# FAQ

## What does this mean?

The amount of keysmash that you have received on your assignment has
increased by one.

## Why did you do this?

There are several reasons I may deem an assignment worthy of keysmash.
These include, but are not limited to:

- I am bad at the assignment
- I did not like the assignment

## Am I banned from the classroom?

Not yet. But you should refrain from making assignments like this in the
future. Otherwise I will be forced to issue additional keysmash, which
may put your assignment privileges in jeopardy.

## I don't believe my assignment deserved keysmash. Can you delete it?

Sure, mistakes happen. But only in exceedingly rare circumstances will I
delete my keysmash. If you would like to issue an appeal, send me an
email explaining what I got wrong. I tend to respond within the day. Do
note, however, that 99.9% of keysmash appeals are rejected, and yours is
likely no exception.

## How can I prevent this from happening in the future?

Accept the keysmash and move on. But learn from this: this type of
assignment will not be tolerated. I will continue to issue keysmash
until you improve your conduct. Remember: making assignments is a
privilege, not a right.
