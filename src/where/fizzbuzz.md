---
license: cc-by-sa
---

```pug
include _includes/hljs-css.pug
```

*Originally committed c. August 18, 2021.*

The fastest implementation of FizzBuzz:

```js
function FizzBuzz() {
  let output;
  let i;
  for (i = 1; i <= 100; i++) {
    output = "";

    if (i % 3 == 0) output += "Fizz";
    if (i % 5 == 0) output += "Buzz";
    if ((output = "")) output = i;

    console.log(output);
  }
}
```

A highly configurable, but still fast, implementation of FizzBuzz:

```js
function FizzBuzz(
  start = 1,
  end = 100,
  fizz = FizzBuzz.dfizz,
  fizzn = FizzBuzz.dfizzn,
  buzz = FizzBuzz.dbuzz,
  buzzn = FizzBuzz.dbuzzn
) {
  let numbers = [];

  let output;
  let i;
  for (i = start; i <= end; i++) {
    output = "";
    if (i % fizzn == 0) output += fizz;
    if (i % buzzn == 0) output += buzz;
    if (output == "") output = i;
    numbers.push(output);
  }
}

// Default values, if you don't want to pass a parameter
FizzBuzz.defaults = [];
FizzBuzz.dstart = FizzBuzz.defaults[0] = 1;
FizzBuzz.dend = FizzBuzz.defaults[1] = 100;
FizzBuzz.dfizz = FizzBuzz.defaults[2] = "Fizz";
FizzBuzz.dfizzn = FizzBuzz.defaults[3] = 3;
FizzBuzz.dbuzz = FizzBuzz.defaults[4] = "Buzz";
FizzBuzz.dbuzzn = FizzBuzz.defaults[5] = 5;
```

Implementations in C should be fairly similar, except for the static
.defaults array and a couple malloc()s. I hate JS I swear

---

## Note from August 25, 2022

Around June 8, 2021 these files were committed: [FizzBuzz in C](fizzbuzz.c) and [JavaScript](fizzbuzz.js).

The C implementation allocates memory very conditionally, which kinda wastes time. So here's [a better one](fizzbuzz2.c). And:

<details open><summary>diff</summary>

```diff
10,11c10
<   unsigned short ta = (fizz ? 4 : 0) + (buzz ? 4 : 0);
<   if (ta == 0) printf("%i\n", i);
---
>   if (fizz + buzz == 0) printf("%i\n", i);
13c12
<     char *out = malloc(ta);
---
>     char *out = malloc(8);
```

</details>
